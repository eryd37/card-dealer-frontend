# card-dealer-frontend

A simple interface that deals 5 cards and shows what type of hand it is.

## How to run

### Run natively

Requires node >=18

1. `npm install`
2. `npm run dev`
3. Go to http://localhost:5173/

## Run with Docker

Requires Docker and Docker Compose

1. `docker-compose up --build`
2. Go to http://localhost:8081/
