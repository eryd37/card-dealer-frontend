/**
 * Mapping between letters and norwegian names of suits:
 *
 * r: Ruter
 *
 * k: Kløver
 *
 * s: Spar
 *
 * h: Hjerter
 */
export enum SUITS {
  "r",
  "k",
  "s",
  "h",
}

export const HAND_SIZE = 5;
