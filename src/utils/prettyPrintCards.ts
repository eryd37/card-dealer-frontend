import type { Card } from "../types/Card";
import { mapRankToString } from "./mapRankToString";

export function prettyPrintCards(cards: Card[]) {
  cards.forEach((card) => {
    console.log(`${card.suit}${mapRankToString(card.rank)}`);
  });
}
