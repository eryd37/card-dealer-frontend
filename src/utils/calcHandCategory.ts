import type { Card } from "@/types/Card";

export function calcHandCategory(hand: Card[]): string {
  if (isFlush(hand) && isStraight(hand)) {
    if (hand.at(-1)?.rank === 14) {
      return "Royal flush";
    }
    return "Straight flush";
  } else if (maxOfAKind(hand) === 4) {
    return "Four of a kind";
  } else if (isFullHouse(hand)) {
    return "Full house";
  } else if (isFlush(hand)) {
    return "Flush";
  } else if (isStraight(hand)) {
    return "Straight";
  } else if (maxOfAKind(hand) === 3) {
    return "Three of a kind";
  } else if (isTwoPair(hand)) {
    return "Two pair";
  } else if (maxOfAKind(hand) === 2) {
    return "Pair";
  }
  return "High card";
}

function isStraight(hand: Card[]) {
  hand.sort((a, b) => a.rank - b.rank);
  let result = true;
  hand.forEach((value, index, array) => {
    if (index > 0) {
      if (value.rank - 1 !== array.at(index - 1)?.rank) {
        result = false;
      }
    }
  });
  return result;
}

function isFlush(hand: Card[]) {
  const firstSuit = hand.at(0)!!.suit;
  let result = true;
  hand.forEach((card) => {
    if (card.suit !== firstSuit) {
      result = false;
    }
  });
  return result;
}

function isTwoPair(hand: Card[]) {
  const counts = countRanks(hand);
  return counts.at(-1)?.count === 2 && counts.at(-2)?.count === 2;
}

function isFullHouse(hand: Card[]) {
  const counts = countRanks(hand);
  return counts.at(-1)?.count === 3 && counts.at(-2)?.count === 2;
}

function maxOfAKind(hand: Card[]) {
  return countRanks(hand).at(-1)!!.count;
}

function countRanks(hand: Card[]) {
  type RankCounter = { count: number; rank: number };
  const counts = hand
    .reduce((prev, current) => {
      const match = prev.find((value) => value.rank === current.rank);
      if (match) {
        match.count += 1;
        return prev;
      } else {
        return prev.concat({ count: 1, rank: current.rank });
      }
    }, [] as RankCounter[])
    .sort((a, b) => a.count - b.count);
  return counts;
}
