import type { Card } from "../types/Card";
import { mapIndexToSuit } from "./mapIndexToSuit";

export function createDeck(): Card[] {
  return Array(52)
    .fill(undefined)
    .map((_, index: number) => {
      const suit = mapIndexToSuit(index);
      const remainder = index % 13;
      const rank = remainder + 2;
      return {
        suit,
        rank,
      };
    });
}
