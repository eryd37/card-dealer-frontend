export function mapRankToString(rank: number): string {
  if (rank === 10) {
    return "t";
  } else if (rank === 11) {
    return "j";
  } else if (rank === 12) {
    return "q";
  } else if (rank === 13) {
    return "k";
  } else if (rank === 14) {
    return "a";
  } else {
    return rank.toString();
  }
}
