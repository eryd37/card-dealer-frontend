import { SUITS } from "../config/constants";
import type { Suit } from "../types/Suit";

export function mapIndexToSuit(index: number): Suit {
  const suitIndex = Math.floor(index / (52 / 4));
  const suit = SUITS[suitIndex];
  return suit as Suit;
}
