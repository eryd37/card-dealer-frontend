import type { Card } from "@/types/Card";
import { calcHandCategory } from "@/utils/calcHandCategory";
import { createDeck } from "@/utils/createDeck";
import { defineStore } from "pinia";

export const useCardStore = defineStore("card", {
  state: () => ({
    deck: [] as Card[],
    hand: [] as Card[],
  }),
  getters: {
    handCategory(): string {
      return calcHandCategory(this.hand);
    },
  },
  actions: {
    resetDeck() {
      this.deck = createDeck();
      return this.deck;
    },
    dealHand(numberOfCards: number) {
      if (numberOfCards > this.deck.length) {
        this.resetDeck();
      }
      let hand: Card[] = [];
      for (let i = 0; i < numberOfCards; i++) {
        const index = Math.floor(Math.random() * this.deck.length);
        hand.push(this.deck.splice(index, 1)[0]);
      }
      this.hand = hand.sort((a, b) => a.rank - b.rank);
      return hand;
    },
  },
});
