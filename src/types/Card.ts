import type { Suit } from "@/types/Suit";

export interface Card {
  suit: Suit;
  rank: number;
}
