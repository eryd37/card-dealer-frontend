/**
 * Mapping between letters and norwegian names of suits:
 *
 * r: Ruter
 *
 * k: Kløver
 *
 * s: Spar
 *
 * h: Hjerter
 */
export type Suit = "r" | "k" | "s" | "h";
